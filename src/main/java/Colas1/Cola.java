/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Colas1;

import javax.swing.JOptionPane;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Cola {
    
    private Nodo inicioCola, finCola;
    int tamano;
    String cola;
    
    public Cola(){
        inicioCola=null;
        finCola=null;
        tamano=0;
    }
    // Valida si la Cola esta Vacia
    public boolean colaVacia(){
        if (inicioCola==null) {
            return true;
        } else {
            return false;
        }
    }
    
    //Inserta Elementos a la Cola
    public void insertarCliente(String cliente){
        Nodo nuevo=new Nodo();
        nuevo.dato=cliente;
        nuevo.siguiente=null;
        if (colaVacia()) {
            inicioCola=nuevo;
        } 
        else {
            finCola.siguiente=nuevo;
        }
        finCola=nuevo;
        tamano++;
    }
    
    public String atenderCliente(){
        if (!colaVacia()) {
            String cliente=inicioCola.dato;
            if (inicioCola==finCola) {
                inicioCola=null;
                finCola=null;
            } else {
                inicioCola=inicioCola.siguiente;
            }
            tamano--;
            return cliente;
        } else {
             return "Cola Vacia";
        }
    }
    
    //Obtener el siguiente a ser Atendido
    public String siguienteAtencion(){
        return inicioCola.dato;
    }
    
    public int tamanoCola(){
        return tamano;
    }
    
    //Mostrar todos los registros de la Cola
    public void mostrarCola(){
        Nodo recorrer = inicioCola;
        String impresion="";
        int orden=0;
        while (recorrer!=null) { 
            cola+=recorrer.dato+" ";
            recorrer=recorrer.siguiente;
        }
        String cadena[]=cola.split(" ");
        for (int i = cadena.length-1; i >= 0; i--) {
            orden=(i+1);
            impresion+=" [ "+ orden + " : "+ cadena[i]+ " ] ";
            
        }
        JOptionPane.showMessageDialog(null, impresion);
        cola="";
    }
    // Vaciar totalmente la Cola
    public void vaciarCola(){
        while (!colaVacia()) {
            atenderCliente();
        }
    }
}
