package Colas1;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        
        LinkedList cola = new LinkedList();
        
        for (int i = 1; i < 15; i++) {
            cola.offer(i);
        }
        
        System.out.println("Primero en ENTRAR: "+ cola.peek());
        System.out.println("Primero en ENTRAR: "+ cola.poll());
        System.out.println("Cola Vacia: "+ cola.isEmpty());
        System.out.println("Primero en ENTRAR: "+ cola.getFirst());
        System.out.println("Primero en ENTRAR: "+ cola.getLast());
        System.out.println("Primero en ENTRAR: "+ cola.peek());
        
    }
    
}
