/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Colas1;

import javax.swing.JOptionPane;

/**
 *
 * @author M@rlon Alvarado
 */
public class Home {

    
    public static void main(String[] args) {
       
        int opcion = 0;
        String cliente;
        Cola cola = new Cola();

        do {
            opcion = Integer.parseInt(
                    JOptionPane.showInputDialog(
                            "Menu de Opciones \n\n"
                            + "1. Ingresar Cliente\n"
                            + "2. Atender Cliente\n"
                            + "3. Mostrar la Fila\n"
                            + "4. Consultar siguiente a atender\n"
                            + "5. Cerrar Oficinas Eliminar cola\n"
                            + "6. Hay alguien en la Fila?\n"
                            + "7. Tamano de la Final\n"
                            + "8. Salir "));
            switch (opcion) {
                case 1:
                    cliente= JOptionPane.showInputDialog(null, "Ingrese el Cliente: ");
                    cola.insertarCliente(cliente);
                    break;
                case 2:
                    if (!cola.colaVacia()) {
                        JOptionPane.showMessageDialog(null, "Se ha Atendido "
                                + cola.atenderCliente());

                    } else {
                        JOptionPane.showMessageDialog(null, "no hay cliente en la Fila");
                    }
                    break;
                case 3:
                    if (!cola.colaVacia()) {
                        cola.mostrarCola();

                    } else {
                        JOptionPane.showMessageDialog(null, "no hay nadie en la Fila ");
                    }
                    break;
                case 4:
                    if (!cola.colaVacia()) {
                        JOptionPane.showMessageDialog(null, "El siguiente en ser atendido es:  "
                                + cola.siguienteAtencion());

                    } else {
                        JOptionPane.showMessageDialog(null, "No hay nadie en la Fila ");
                    }
                    break;
                case 5:
                    if (!cola.colaVacia()) {
                        cola.colaVacia();

                    } else {
                        JOptionPane.showMessageDialog(null, "No hay nadie ");
                    }
                    break;
                case 6:
                    if (!cola.colaVacia()) {
                        JOptionPane.showMessageDialog(null, "Hay Fila ");

                    } else {
                        JOptionPane.showMessageDialog(null, "No hay Fila ");
                    }
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "Existen " + cola.tamanoCola() + " Clientes pedientes de atencion ");
                    break;
                case 8:
                    System.exit(0);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion Incorrecta ");
                    break;
            }
        } while (opcion != 0);

    }
    
}
